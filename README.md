# This is my README
To run the src code, first you need to get to pox directory,, copy files ext into ext directory or simply replace ext directory, and at pox directory
type ./pox.py new_hedera_eval


and then get to mini net and type down sudo python hederaMininet_backup.py  -b 10 -k 4 -t 75 --traffic stride


finally after mini net exits, you can kill hedera and at mini net directory, type bash plot_run_summary.sh , and it is gonna give you a set of graphs based on our bandwidth monitor.


There are different measurement, and default is primary_backup, the time difference is 5 seconds, i.e. it takes 5 seconds to detect the failure and backup kicks in. 
If you want to change to other types of failure recovery, go to _global_first_fit function in new_hedera_eval and make some trivial changes. :)

hedera code is in pox/ext/


 