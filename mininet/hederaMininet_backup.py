#course 518 project, 
#author: Kelvin Zou
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.log import lg, output
from mininet.node import CPULimitedHost, RemoteController, Host
from mininet.link import TCLink
from mininet.util import irange, custom, quietRun, dumpNetConnections
from mininet.cli import CLI
from time import sleep, time
from multiprocessing import Process
from subprocess import Popen
import random

import argparse

import sys
import os
import signal
from time import time

from util.monitor import monitor_devs_ng


parser = argparse.ArgumentParser(description="Hedera tests")
parser.add_argument('--bw', '-b',
                    type=float,
                    help="Bandwidth of network links",
                    required=True)

parser.add_argument('--dir', '-d',
                    help="Directory to store outputs",
                    default="results")

parser.add_argument('-k',
                    type=int,
                    help=("Number of ports per switch in FatTree."
                    "Must be >= 1"),
                    required=True)

parser.add_argument('--time', '-t',
                    dest="time",
                    type=int,
                    help="Duration of the experiment.",
                    default=5)

parser.add_argument('--traffic',
                    dest="traffic",
                    help="Traffic matrix to simulate",
                    default="stride,1")

parser.add_argument('--seed',
                    dest="seed",
                    help="Random number generator seed",
                    type=int)

parser.add_argument('--controller',
                    dest="controller",
                    help="Controller shell command")

parser.add_argument('--control',
                    dest="control",
                    help="Run control network",
                    default=False,
                    action="store_true")

parser.add_argument('--flowsPerHost',
                    dest="fph",
                    type=int,
                    help="Only use this parameter with random traffic pattern",
                    default=1)


args = parser.parse_args()

IPERF_PATH="/usr/bin/iperf"

# assert args.controller or args.control
assert(os.path.exists(IPERF_PATH))

if not os.path.exists(args.dir):
    os.makedirs(args.dir)

class FatTreeTopo ( Topo ):
    '''
        Three-layer homogeneous Fat Tree.
        From "A scalable, commodity data center network architecture, M. Fares et
        al. SIGCOMM 2008."
        
        Taken from Ripl. Updated by Kelvin Zou
    '''
    def __init__(self, k = 4, speed = 1.0):
        super( FatTreeTopo, self ).__init__()
        # Build topology
        self.addTree( k, speed )
    #from this point until the end of next pond sign, it is all fat tree and should be all correct.

    def addTree(self, k, speed):
        '''
        @param k switch degree
        @param speed bandwidth in Gbps
        '''
        numPods = k
        aggPerPod = k / 2
        pods = range(0, k)
        core_sws = range(1, k / 2 + 1)
        agg_sws = range(k / 2, k)
        edge_sws = range(0, k / 2)
        hosts = range(0, k / 2 )
        switch_num = 1
        host_num = 1
        id_gen = "10.{0}.{1}.{2}"
        map_ftid_to_normalid = {}
        map_fthostid_to_normalid = {}
        map_and_count_port = {}
        for p in pods:
            for e in edge_sws:
                edge_id = id_gen.format(p, e, 1)
                if edge_id not in map_ftid_to_normalid:
                    self.addSwitch('s%s' % switch_num)
                    map_ftid_to_normalid[edge_id] = 's%s' % switch_num
                    print "Edge switch %s" % (switch_num)
                    switch_num += 1

                for h in hosts:
                    host_id = id_gen.format(p, e, (h+2))
                    if host_id not in map_fthostid_to_normalid:
                        self.addHost('h%s' % (host_num))
                        map_fthostid_to_normalid[host_id] = 'h%s' % (host_num)
                        print "Host is %s" % (host_num)
                        host_num += 1
                    self.addLink(map_fthostid_to_normalid[host_id], map_ftid_to_normalid[edge_id])
                    if edge_id not in map_and_count_port:
                        map_and_count_port[edge_id]=1
                    else:
                        map_and_count_port[edge_id] +=1
                    print "add link btwn switch %s at port %s and host %s at port 1" %(map_ftid_to_normalid[edge_id], map_and_count_port[edge_id],map_fthostid_to_normalid[host_id])

        for p in pods:
            for e in edge_sws:
                #regenerate the id to get the switch number
                edge_id = id_gen.format(p, e, 1)
                for a in agg_sws:
                    agg_id = id_gen.format(p, a, 1)
                    if agg_id not  in map_ftid_to_normalid:
                        self.addSwitch('s%s' % switch_num)
                        map_ftid_to_normalid[agg_id] = 's%s' % switch_num
                        print "Agg switch %s" % (switch_num)
                        switch_num += 1
                    self.addLink(map_ftid_to_normalid[edge_id], map_ftid_to_normalid[agg_id])
                    if edge_id not in map_and_count_port:
                        map_and_count_port[edge_id]=1
                    else:
                        map_and_count_port[edge_id] +=1
                    if agg_id not in map_and_count_port:
                        map_and_count_port[agg_id]=1
                    else:
                        map_and_count_port[agg_id] +=1
                    print "add link btwn aggr switch %s at port %s and edge switch %s at port %d" %(map_ftid_to_normalid[agg_id],  map_and_count_port[agg_id], map_ftid_to_normalid[edge_id], map_and_count_port[edge_id])

        for p in pods:
            for a in agg_sws:    
                #regenerate the id to get the switch number
                agg_id = id_gen.format(p, a, 1)
                c_index = a - k/2 + 1
                for c in core_sws:
                    core_id = id_gen.format(k, c_index,c)
                    if core_id not in map_ftid_to_normalid:
                        self.addSwitch('s%s' % switch_num)
                        print "core switch %s" % (switch_num)
                        map_ftid_to_normalid[core_id] = 's%s' % switch_num
                        switch_num += 1
                    self.addLink(map_ftid_to_normalid[core_id], map_ftid_to_normalid[agg_id])
                    if agg_id not in map_and_count_port:
                        map_and_count_port[agg_id]=1
                    else:
                        map_and_count_port[agg_id] +=1
                    if core_id not in map_and_count_port:
                        map_and_count_port[core_id]=1
                    else:
                        map_and_count_port[core_id] +=1
                    print "add link btwn core switch %s at port %s and aggr switch  %s at port %s " %(map_ftid_to_normalid[core_id] ,map_and_count_port[core_id] ,map_ftid_to_normalid[agg_id], map_and_count_port[agg_id])


# Begin traffic pettern #######################
# this means all hosts are talking to some neighbour with stride distance, e.g. h1->h5, h2->h6, ... h15->h4
def compute_stride(k, stride):
    matrix = []
    for src_index in range(k**3 / 4):
        matrix.append((src_index + stride) % (k**3 / 4)+1)
    print matrix
    return matrix

# each node sending to random node, could have two nodes sending to the same one node, e.g. h1->h4, h3->h4, h5->h4
def compute_random(k):
    matrix = []
    nHosts = (k**3)/4
    for ind in range(1, nHosts + 1):
        dst = random.randint(1, nHosts)
        while dst == ind:
            dst = random.randint(1, nHosts)
        matrix.append(dst)
    print matrix
    return matrix

#this is each bi section and should not have any two sending one node, 
#always one client talking to one server and one server talking to one client, strictly pairwise.

def compute_randbij(k):
    matrix = range(1, (k**3)/4+1 )
    nHosts = (k**3)/4
    random.shuffle(matrix)
    for i in range(len(matrix)):
        if matrix[i] ==(i+1):
            dst = random.randint(1, nHosts)
            while dst == (i+1):
                dst = random.randint(1, nHosts)
            matrix[dst-1] = i +1
            matrix[i] = dst
    
    print matrix
    return matrix

# End traffic pattern ########################



#running traffic part

def start_tcpprobe():
    os.system("rmmod tcp_probe 1>/dev/null 2>&1; modprobe tcp_probe")
    Popen("cat /proc/net/tcpprobe >/dev/null", shell=True)

def stop_tcpprobe():
    os.system("killall -9 cat; rmmod tcp_probe 1>/dev/null 2>&1")

def wait_listening(client, server, port):
    "Wait until server is listening on port"
    if not 'telnet' in client.cmd('which telnet'):
        raise Exception('Could not find telnet')
    print server.IP()
    cmd = ('sh -c "echo A | telnet -e A %s %s"' %
                 (server.IP(), port))
    while 'Connected' not in client.cmd(cmd):
        output('waiting for', server,
                     'to listen on port', port, '\n')
        sleep(.5)

def run_expt(net, k, flowsToCreate):
    "Run experiment"

    seconds = args.time
    
    port = 5001
    srcout =''
    destout = ''
    # Start receivers
    dstSet = set([p[1] for p in flowsToCreate])
    print dstSet
    for src_index, dest_index in flowsToCreate:
        print 'sending from %s to %s' %( (src_index+1), dest_index)
    
    for dest_index in dstSet:
        print '%s is starting server iperf ' %dest_index
        dest = net.getNodeByName('h%s'%dest_index)
        destout = dest.cmd('%s -s -p %s -a 2 > /dev/null &' % (IPERF_PATH, port))
    print flowsToCreate
    for src_index, dest_index in flowsToCreate:
        src = net.getNodeByName('h%s'%(src_index+1))
        dest = net.getNodeByName('h%s'%dest_index)

        wait_listening(src, dest, port)
    
    print "Listeners waiting"

    # Start the bandwidth and cwnd monitors in t1he background
    
    monitor = Process(target=monitor_devs_ng, args=('%s/bwm.txt' % args.dir, 1.0))
    monitor.start()

    start_tcpprobe()
  
    # Start the senders
    for src_index, dest_index in flowsToCreate:
        src = net.getNodeByName('h%s'%(src_index+1))
        dest = net.getNodeByName('h%s'%dest_index)
        srcout = src.cmd('%s -c %s -p %s -t %d -i 1 -yc > /dev/null &' % (IPERF_PATH, dest.IP(), port, seconds))

    print "Senders sending"

    for i in range(seconds):
        print "%d s elapsed" % i
        sleep(1)

    print "Ending experiment"
    os.system('killall -9 ' + IPERF_PATH)

    # Shut down monitors
    print "Waiting for monitor to stop"
    os.system('killall -9 bwm-ng')
    stop_tcpprobe()
    
def addMatrixToFlow(flowToCreate, matrix):
    for i in range(len(matrix)):
        flowToCreate.append((i, matrix[i]))

def check_prereqs():
    "Check for necessary programs"
    prereqs = ['telnet', 'bwm-ng', 'iperf', 'ping']
    for p in prereqs:
        if not quietRun('which ' + p):
            raise Exception((
                'Could not find %s - make sure that it is '
                'installed and in your $PATH') % p)





def create_elephant_flows(flowsToCreate):
   f = open('../pox/elephants.txt', 'w')
   elephant_flows = []
   for (src, dst) in flowsToCreate:
    src_id = src+1
    dst_id = dst
    durationtime = 90

    elephant_flows.append((src_id, dst_id, durationtime))
   print "creating elephant flows with iperf\n"
   print "here is the designed list of flows:\n"
   print elephant_flows
   print str(time())
   f.write( str(time()) +'\n' )

   for flow in elephant_flows:
    print>>f, flow
   f.close()
   return






def main():
    print "start running!"
    print str(time())
    k = args.k
    host = custom(CPULimitedHost, cpu=4.0/(k**3))
    link = custom(TCLink, bw=args.bw, delay='0ms')

    topo = FatTreeTopo(k=k)
    if args.controller:
        controller = Popen(args.controller, shell=True, preexec_fn=os.setsid)
        #switch = OVSSwitch,
    net = Mininet(topo=topo, controller= RemoteController,  host=host, link=link, build=True, cleanup=True, autoPinCpus=True, autoSetMacs=True, listenPort = 6633)
    net.start()
    

    flowsToCreate = []    
    for fcount in range(args.fph):
        if args.traffic.startswith('random'):
            matrix = compute_random(k)
        elif args.traffic.startswith('randbij'):
            matrix = compute_randbij(k)
        elif args.traffic.startswith('stride'):
            matrix = compute_stride(k, 8)
        else:
            raise Exception('Unrecognized traffic type')
        print "Running with matrix"
    
    addMatrixToFlow(flowsToCreate, matrix)
    print flowsToCreate
    create_elephant_flows(flowsToCreate)
    #net.pingPair()
    #sleep(5)
    
    for (i, j) in flowsToCreate:
        hosts = [ net.hosts[i], net.hosts[j-1] ]  
        net.ping(hosts)

    run_expt(net, k, flowsToCreate)
    net.iperf(hosts, 'TCP', '10M')

    if args.controller:
        os.killpg(controller.pid, signal.SIGKILL)
    
    net.stop()
    
if __name__ == '__main__':
    check_prereqs()
    main()
