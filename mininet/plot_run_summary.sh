#!/bin/bash


dir='./'
k=4
maxy=350
km1=$((k - 1))
kdiv2=$((km1 / 2))


cat results/bwm.txt |tail -n +84 >bwm.txt

python util/plot_rate.py \
       -f $dir/bwm.txt \
       --legend Hedera ECMP Nonblocking Random ST \
       --maxy $maxy \
       --xlabel 'Time (s)' \
       --ylabel 'Rate (Mbps)' \
       --total \
       -i "(([0-$km1]_[0-$kdiv2]_1-eth\d*[24680])|(s[1-8]-eth.*))" \
       -o $dir/summaryEdge.png

python util/plot_rate.py \
-f $dir/bwm.txt \
--legend Hedera ECMP Nonblocking Random ST \
--maxy $maxy \
--xlabel 'Time (s)' \
--ylabel 'Rate (Mbps)' \
--total \
-i "(([0-$km1]_[0-$kdiv2]_1-eth\d*[24680])|(s([9]|1[0-6])-eth.*))" \
-o $dir/summaryAggre.png

python util/plot_rate.py \
-f $dir/bwm.txt \
--legend Hedera ECMP Nonblocking Random ST \
--maxy $maxy \
--xlabel 'Time (s)' \
--ylabel 'Rate (Mbps)' \
--total \
-i "(([0-$km1]_[0-$kdiv2]_1-eth\d*[24680])|(s(1[7-9]|20)-eth.*))" \
-o $dir/summaryCore.png


python util/plot_rate.py \
-f $dir/bwm.txt \
--legend Hedera ECMP Nonblocking Random ST \
--maxy $maxy \
--xlabel 'Time (s)' \
--ylabel 'Rate (Mbps)' \
--total \
-i "(([0-$km1]_[0-$kdiv2]_1-eth\d*[24680])|(s([3]|[4]|[11]|[12])-eth.*))" \
-o $dir/summaryNoAffectedPod.png

python util/plot_rate.py \
-f $dir/bwm.txt \
--legend Hedera ECMP Nonblocking Random ST \
--maxy $maxy \
--xlabel 'Time (s)' \
--ylabel 'Rate (Mbps)' \
--total \
-i "(([0-$km1]_[0-$kdiv2]_1-eth\d*[24680])|(s([1]|[2]|[9]|[10])-eth.*))" \
-o $dir/summaryFailPod.png

