# Jennifer Gossels
# December 7, 2013

'''
To Do:
1.  Change default values of optional arguments to be more reasonable.
'''

# constants
# Link types
SS = "ss"
HS = "hs"
SH = "sh"

import copy

class Link:
    '''
    A link abstraction.
    '''

    def __init__(self, src, dst, src_port=0, dst_port=0, capacity=10, \
                     current_use=0, owner=None, type=SS):
        '''
        Create Link.  Link is defined by the pair (src, dst), represented as integers.
        Optional arguments include src_port, dst_port, capacity, current use, the controller
        in charge of the link, and the type of link (host --> switch, switch --> host,
        switch --> switch).  Type is switch --> switch (SS) by default.  If type is HS, then
        src_port is -1 (because we don't specify different ports on the hosts.  If type is SH,
        then dst_port is -1.
        '''
        self._src = src
        self._dst = dst
        self._src_port = src_port
        self._dst_port = dst_port
        self._capacity = capacity
        self._current_use = current_use
        self._owner = owner
        self._type = type
        if type == HS:
            self._src_port = -1
        if type == SH:
            self._dst_port = -1

    def src(self):
        return self._src
 
    def dst(self):
        return self._dst

    def src_port(self):
        return self._src_port

    def set_src_port(self, new_src_port):
        self._src_port = new_src_port
         
    def dst_port(self):
        return self._dst_port

    def set_dst_port(self, new_dst_port):
        self._dst_port = new_dst_port

    def capacity(self):
        return self._capacity

    def set_capacity(self, new_capacity):
        self._capacity = new_capacity

    def current_use(self):
        return self._current_use

    def set_current_use(self, new_current_use):
        self._current_use = new_current_use

    def owner(self):
        return self._owner

    def set_owner(self, new_owner):
        self._owner = new_owner

    def type(self):
        return self._type

    def set_type(self, new_type):
        self._type = new_type

    def reverse(self, bidirectional):
        '''
        A NEW Link with (src, dst) = (self.dst, self.src) and (src_port, dst_port) =
        (self.dst_port, self.src_port).  Also, new Link has same owner and the appropriate
        type, based on self.type.  If bidirectional is true, copy usage and capacity settings,
        also.
        '''
        reversed = Link(self.dst(), self.src(), src_port=self.dst_port(), dst_port=self.src_port(), \
                            owner=self.owner())
        if self.type() == HS:
            reversed.set_type(SH)
        if self.type() == SH:
            reversed.set_type(HS)
        if bidirectional:
            reversed.set_capacity(self.capacity())
            reversed.set_current_use(self.current_use())
        return reversed

    def __eq__(self, other):
        '''
        Two Links considered equal if they have the same (src, dst) and are
        of the same type (because host numbering overlaps with switch numbering).
        Not considered equal if self has (src, dst) and other has (dst, src).
        '''
        return isinstance(other, Link) and (self.src() == other.src()) \
            and (self.dst() == other.dst()) and (self.type() == other.type())

    def __repr__(self):
        s = "Link(" + str(self.src()) + ", " + str(self.dst()) + \
            ", src_port=" + str(self.src_port()) + ", dst_port=" + str(self.dst_port()) + \
            ", capacity=" + str(self.capacity()) + ", current_use=" + str(self.current_use()) \
            + ", owner=" + str(self.owner()) + ", type=" + self.type() + ")"
        return s

    def __str__(self):
        src = "(" + str(self.src()) + ", " + str(self.src_port()) + ")"
        dst = "(" + str(self.dst()) + ", " + str(self.dst_port()) + ")"
        if self.type() == HS:
            src = "HOST" + "(" + str(self.src()) + ", -)"
        if self.type() == SH:
            dst = "HOST" + "(" + str(self.dst()) + ", -)"
        lnk = src + " -----> " + dst + "\n"
        cap = "Capacity:  " + str(self.capacity()) + "\n"
        use = "Current Use:  " + str(self.current_use()) + "\n"
        owner = "Owner:  " + str(self.owner()) + "\n\n"
        return lnk + cap + use + owner
  
class GlobalLink(Link):
    '''
    A Link to be stored in a controller's traffic matrix and keep track of
    the flow IDs of all flows currently sending traffic over it.
    '''

    def __init__(self, src, dst, src_port=0, dst_port=0, capacity=0,
                 current_use=0, flows=[], owner=None, type=SS):
        Link.__init__(self, src, dst, src_port, dst_port, capacity,
                                         current_use, owner, type)
        self._flows = flows

    def flows(self):
        return copy.deepcopy(self._flows)

    def set_flows(self, new_flows):
        self._flows = new_flows

    def __repr__(self):
        s = "GlobalLink(" + str(self.src()) + ", " + str(self.dst()) + \
            ", src_port=" + str(self.src_port()) + ", dst_port=" + str(self.dst_port()) + \
            ", capacity=" + str(self.capacity()) + ", current_use=" + str(self.current_use()) \
            + ", flows=" + str(self.flows()) + ", owner=" + str(self.owner()) + ", type=" + \
            self.type() + ")"
        return s

    def __str__(self):
        src = "(" + str(self.src()) + ", " + str(self.src_port()) + ")"
        dst = "(" + str(self.dst()) + ", " + str(self.dst_port()) + ")"
        if self.type() == HS:
            src = "HOST" + "(" + str(self.src()) + ", -)"
        if self.type() == SH:
            dst = "HOST" + "(" + str(self.dst()) + ", -)"
        lnk = src + " -----> " + dst + "\n"
        cap = "Capacity:  " + str(self.capacity()) + "\n"
        use = "Current Use:  " + str(self.current_use()) + "\n"
        flows = "Flows:  " + str(self.flows()) + "\n"
        owner = "Owner:  " + str(self.owner()) + "\n\n"
        return lnk + cap + use + flows + owner
