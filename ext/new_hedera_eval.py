#Edited on Sunday Jan 12, requested BW for Global Firt Fit= 10

#Mojgan Ghasemi

#added periodic check for elephant flows

#added dictionaries to keep track of the time that elephant flows are finished 

#-> no more scheduling of them->remove from the list of input to GFF after finish_time reached

#maintaing a dictionary of traffic and the links used so far 

#modified GFF to work with this controller



# Edited on friday Jan 10 -Adding Elephant Flow Detection and Handling



# Mojgan Ghasemi

# semi-static ARP

# higher priority for hedere rules over ECMP

# Adding timer for schuler for every T_schedule sec

# we'll check for elephent flows every T_schedule sec

# flows that have a duration>MINIMUM_... are considered elephant flow...can change







# TO DO:

# keeping track of old elephant flows and new ones to remove double consideration->how many times feed into GFF? until alive or just once? if until alive ->keep track of each of them getting expired.









# Edited on Thursday Jan 9

# Mojgan Ghasemi - Adding notifier between the detector(measurememnt module) and the controller



# add a hook to the elephants.txt file to get notified when a new elephant flow has been detected

# flows will be conerted to a list of links

# list of elephant flows will be fed into global first fit

# the result will get installed

# higher priority

# replce in the table

T_schedule  = 38

# Copyright Kelvin Zou, modified from James version

# Author: Kelvin Zou

# Created Nov 21, 2013

# purpose: This file is for course project COS518, and in this file, 

# Instead of flooding if the packet in unknown to the table, I tricked it

# to some certain ports based on the known topology.

# And then it sets up its own mac learning table

# this program is aimed to minimize the change in the original file with support of LOOP and ECMP

#





# Copyright 2011,2012 James McCauley

#

# This file is part of POX.

#

# POX is free software: you can redistribute it and/or modify

# it under the terms of the GNU General Public License as published by

# the Free Software Foundation, either version 3 of the License, or

# (at your option) any later version.

#

# POX is distributed in the hope that it will be useful,

# but WITHOUT ANY WARRANTY; without even the implied warranty of

# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the

# GNU General Public License for more details.

#

# You should have received a copy of the GNU General Public License

# along with POX.  If not, see <http://www.gnu.org/licenses/>.

from threading import Timer, Lock

import os

from pox.core import core

import pox

log = core.getLogger()

from pox.lib.util import dpid_to_str

from pox.lib.util import str_to_bool

from pox.lib.packet.ethernet import ethernet, ETHER_BROADCAST

from pox.lib.packet.ipv4 import ipv4

from pox.lib.packet.arp import arp

from pox.lib.addresses import IPAddr, EthAddr

from pox.lib.util import str_to_bool, dpidToStr

from pox.lib.recoco import Timer

import pox.openflow.libopenflow_01 as of

from pox.lib.revent import *

from ecmp import *

import random

from time import sleep, time

import math

import time

import copy

import link

from pprint import pprint

from precomputedHederaLinks import create_best_path



# Timeout for flows

FLOW_IDLE_TIMEOUT = 10000

FLOW_PERMANENT_TIMEOUT = 10000

# Timeout for ARP entries

ARP_TIMEOUT = 200



# Maximum number of packet to buffer on a switch for an unknown IP

MAX_BUFFERED_PER_IP = 15



# Maximum time to hang on to a buffer for an unknown IP in seconds

MAX_BUFFER_TIME = 5



class Entry (object):

  """

  Not strictly an ARP entry.

  We use the port to determine which port to forward traffic out of.

  We use the MAC to answer ARP replies.

  We use the timeout so that if an entry is older than ARP_TIMEOUT, we

   flood the ARP request rather than try to answer it ourselves.

  """

  def __init__ (self, port, mac):

    self.timeout = time.time() + ARP_TIMEOUT

    self.port = port

    self.mac = mac



  def __eq__ (self, other):

    if type(other) == tuple:

      return (self.port,self.mac)==other

    else:

      return (self.port,self.mac)==(other.port,other.mac)

  def __ne__ (self, other):

    return not self.__eq__(other)



  def isExpired (self):

    if self.port == of.OFPP_NONE: return False

    return time.time() > self.timeout





def dpid_to_mac (dpid):

  return EthAddr("%012x" % (dpid & 0xffFFffFFffFF,))



max_BW = 10

class Hedera_over_ECMP (EventMixin):



  def __init__ (self, fakeways = [], arp_for_unknowns = False):

    # These are "fake gateways" -- we'll answer ARPs for them with MAC

    # of the switch they're connected to.

    self.fakeways = set(fakeways)

    self.waiting = True

    self.has_GFF_ran = 0

    self.isFirst = True
    '''
    if os.path.exists("elephants.txt"):
      os.remove("elephants.txt")
    '''
    self.arp_for_unknowns = arp_for_unknowns
    self.switchCount =0
    self.outstanding_arps = {}

    self.tuple= {}

    # (dpid,IP) -> [(expire_time,buffer_id,in_port), ...]

    # These are buffers we've gotten at this datapath for this IP which

    # we can't deliver because we don't know where they go.

    self.lost_buffers = {}



    # For each switch, we map IP addresses to Entries



    self.route = StructuredRouting(4)

 #   timer = Timer(5, self._RequestStats)

    timer3 = Timer(T_schedule, self._call_GFF)
    print "**************************************timer start!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"
    timer3.start()

 #   timer.start()

    # things needed for elephant flow detectiond and rescheduling

    #traffic is a dict that keeps track of all Switch to Switch links that are used availabe and if used, how much of theri BW is used. 

    self.latest_detection_time = ''

    self.list_of_elephants = []

    self.input_to_gff = []

    self.dict_of_ECMP_paths = {}

    self.dict_of_hedera_paths = {}

    for i in range(1, 16):

      for j in range(i+1, 17):

        (links, revLinks) = self.route.get_bidirectional_path(i, j)

        key = (i,j)

        value = links

        key2 = (j,i)

        value2 = revLinks

        self.dict_of_ECMP_paths[key] = value

        self.dict_of_ECMP_paths[key2] = value2

    
    # This timer handles expiring stuff

    self._expire_timer = Timer(5, self._handle_expiration, recurring=True)



    self.listenTo(core)







  def _install_rules_on_new_path(self, src_host, dst_host,links, t_hard_out,priority):

    print "installing Hedera rules for the elephant flow from %d to %d ob both directions\n" %(src_host,dst_host)

   
    id_gen = "10.0.0.{0}"

    srcIP = id_gen.format(src_host)

    dstIP = id_gen.format(dst_host)

  
    #switch Ids 

    s1_id = links[1].src()

    s2_id = links[2].src()

    s3_id = links[3].src()

    s4_id = links[4].src()

    s5_id = links[5].src()

    #in_port and ouports

    s1_inport = links[0].dst_port()

    s1_outport = links[1].src_port()



    s2_inport = links[1].dst_port()

    s2_outport = links[2].src_port()



    s3_inport = links[2].dst_port()

    s3_outport = links[3].src_port()



    s4_inport = links[3].dst_port()

    s4_outport = links[4].src_port()



    s5_inport =links[4].dst_port()

    s5_outport = links[5].src_port()

    f = open("rulesInstalled.txt", "a")



    #installing rules reverse way....

    self._install_rule(srcIP, dstIP, s5_id, s5_inport, s5_outport, priority, t_hard_out, src_host)

    f.write(str(s5_id)+" "+str(srcIP)+' '+str(dstIP)+"\n" )   

    self._install_rule(srcIP, dstIP, s4_id, s4_inport, s4_outport, priority, t_hard_out, src_host)

    f.write(str(s4_id)+" "+str(srcIP)+' '+str(dstIP)+"\n" )   

    self._install_rule(srcIP, dstIP, s3_id, s3_inport, s3_outport, priority, t_hard_out, src_host)

    f.write(str(s3_id)+" "+str(srcIP)+' '+str(dstIP)+"\n" )   

    self._install_rule(srcIP, dstIP, s2_id, s2_inport, s2_outport, priority, t_hard_out, src_host)

    f.write(str(s2_id)+" "+str(srcIP)+' '+str(dstIP)+"\n" )   

    self._install_rule(srcIP, dstIP, s1_id, s1_inport, s1_outport, priority, t_hard_out, src_host)

    f.write(str(s1_id)+" "+str(srcIP)+' '+str(dstIP)+"\n" )   

 

    f.write("--------------------------------------\n")

    f.close()

  def isInPod1(self, srcID, dstID):
    if srcID<9 and srcID>4:
      return True
    else:
      return False

  def _global_first_fit(self, elephants):

    print "inside global first_first_fit for this list of elephant flows"

    print elephants
    time_out_for_hedera_rule = random.randint(33, 38)
    timeout_perm = 1000
    timeout_pod = 30
    for (src_host, dst_host, requested_bw) in elephants:

      path = create_best_path(src_host, dst_host)

      reverse_path  = create_best_path(dst_host, src_host)

      if self.isInPod1(src_host, dst_host):
        print "do we ever have short time out!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        time_out_for_hedera_rule = timeout_pod
        self._install_rules_on_new_path(src_host, dst_host, path, time_out_for_hedera_rule, 2)
      else:
        time_out_for_hedera_rule = timeout_perm
        self._install_rules_on_new_path(src_host, dst_host, path, time_out_for_hedera_rule, 2)

      time_out_for_hedera_rule  = timeout_perm
      self._install_rules_on_new_path(dst_host, src_host, reverse_path, time_out_for_hedera_rule, 2)

      self.dict_of_hedera_paths[(src_host,dst_host)] = path

      self.dict_of_hedera_paths[(dst_host,src_host)] = reverse_path

        

                          
  

  def _call_GFF(self):

    print "!!!!!!!!!!!!!!$$$$$$$$$$$$$$$$$!!!inside GFF caller!!!!!!$$$$$$$$$$!!!!\n"
    timer3= Timer(T_schedule, self._call_GFF)
    elephants = []

    for flow in self.list_of_elephants:
      elephants.append((flow['src'],flow['dst'],flow['required_BW']))

    print "the list is ready, calling GFF now"

    self._global_first_fit(elephants)
    timer3.start()
    return 

  

  




  def _handle_expiration (self):

    # Called by a timer so that we can remove old items.

    empty = []

    for k,v in self.lost_buffers.iteritems():

      dpid,ip = k



      for item in list(v):

        expires_at,buffer_id,in_port = item

        if expires_at < time.time():

          # This packet is old.  Tell this switch to drop it.

          v.remove(item)

          po = of.ofp_packet_out(buffer_id = buffer_id, in_port = in_port)

          core.openflow.sendToDPID(dpid, po)

      if len(v) == 0: empty.append(k)



    # Remove empty buffer bins

    for k in empty:

      del self.lost_buffers[k]



  def _send_lost_buffers (self, dpid, ipaddr, macaddr, port):

    """

    We may have "lost" buffers -- packets we got but didn't know

    where to send at the time.  We may know now.  Try and see.

    """

    if (dpid,ipaddr) in self.lost_buffers:

      # Yup!

      bucket = self.lost_buffers[(dpid,ipaddr)]

      del self.lost_buffers[(dpid,ipaddr)]

      log.debug("Sending %i buffered packets to %s from %s"

                % (len(bucket),ipaddr,dpidToStr(dpid)))

      for _,buffer_id,in_port in bucket:

        po = of.ofp_packet_out(buffer_id=buffer_id,in_port=in_port)

        po.actions.append(of.ofp_action_dl_addr.set_dst(macaddr))

        po.actions.append(of.ofp_action_output(port = port))

        core.openflow.sendToDPID(dpid, po)



  def install_ECMP(self):

    id_gen = "10.0.0.{0}"

    for dpid in range(1, 21 ):



      for i in range(1, 16):

        for j in range(i+1, 17):

          srcIP = id_gen.format(i)

          dstIP = id_gen.format(j)

          if (dpid, i, j) in self.lookUpTable:

            (inport, outport, dontcare ) = self.lookUpTable[(dpid, i, j)]

            self._install_rule(srcIP , dstIP, dpid, inport, outport, 1, 100)

      sleep(0.5)



  def _handle_GoingUpEvent (self, event):

    self.listenTo(core.openflow)

   

    log.debug("Up... ")









  def _install_rule2(self, event, srcIP, dstIP, dpID, inport, outport, priority, t_hard_out, buffer_id):

    actions = []

    actions.append(of.ofp_action_output(port = outport))

    srcIP_and_Mask = srcIP + "/32"

    dstIP_and_Mask = dstIP + "/32"



    cus_match = of.ofp_match()

    cus_match.nw_src = srcIP_and_Mask

    cus_match.nw_dst = dstIP_and_Mask

    cus_match.in_port = inport

    log.debug ("match is %s", cus_match)



    msg = of.ofp_flow_mod(command=of.OFPFC_ADD,

                          idle_timeout=FLOW_IDLE_TIMEOUT,

                          hard_timeout=t_hard_out,

                          priority = priority,

                          buffer_id = buffer_id,

                          actions=actions,

                          match=cus_match )

    core.openflow.sendToDPID(dpID, msg)





  def _install_rule(self, srcIP, dstIP, dpID, inport, outport, priority, t_hard_out, srcID):

    actions = []

    actions.append(of.ofp_action_output(port = outport))

    srcIP_and_Mask = srcIP + "/32"

    dstIP_and_Mask = dstIP + "/32"

    id_gen = "00:00:00:00:00:{0}"

    src_mac = id_gen.format(hex(srcID).split('x')[1])



    cus_match = of.ofp_match()

    cus_match.nw_src = srcIP_and_Mask

    cus_match.nw_dst = dstIP_and_Mask

    cus_match.in_port = inport

    log.debug ("match is %s", cus_match)

    cus_match.dl_src =  EthAddr(src_mac)

    msg = of.ofp_flow_mod(command=of.OFPFC_ADD,

                          idle_timeout=FLOW_IDLE_TIMEOUT,

                          hard_timeout=t_hard_out,

                          priority = priority,

                          actions=actions,

                          match=cus_match )

    core.openflow.sendToDPID(dpID, msg)



  def _get_ECMP_path(self, srcID, dstID):

      path = self.dict_of_paths[(srcID, dstID)]

#one directional

  def install_only_hedera(self, srcID, dstID):

      path = create_best_path(srcID, dstID)

      self._install_rules_on_new_path(srcID, dstID, path, 10,2)



  def install_all_hedera(self):

      for i in range (1,17):

	for j in range (1,17):

	   if i == j :

		continue

	   path = create_best_path(i,j)

	   self._install_rules_on_new_path(i,j, path, 10, 2)



  def install_all_ECMP(self, time_out):

     for i in range (1,17):

	for j in range (1,17):

	   path = self.dict_of_ECMP_paths[(i,j)]

	   self.install_rules_on_new_path(i,j,path, time_out) 

#one directional

  def install_only_ECMP(self, srcID, dstID, time_out):

      path = self.dict_of_ECMP_paths[(srcID,dstID)]

      self._install_rules_on_new_path(srcID, dstID, path, time_out, 1)

    

  def _handle_PacketIn (self, event):

    dpid = event.connection.dpid

    inport = event.port

    packet = event.parsed
    ECMP_timeout = 1000

    if not packet.parsed:

      log.debug("%i %i ignoring unparsed packet", dpid, inport)

      return



    if isinstance(packet.next, ipv4):

      print "######################################is it IP######################################################"

      

      log.debug ("%i %i IP %s => %s", dpid,inport,

                packet.next.srcip,packet.next.dstip)

      

      #starting from here we install the rules directly

      a = packet.next

      srcHost = a.srcip.toSigned() & 0x00ffffff

      dstHost = a.dstip.toSigned() & 0x00ffffff
      if (srcHost, dstHost) not in self.tuple:
        self.tuple [(srcHost, dstHost)]=1
        self.list_of_elephants.append({'src':srcHost, 'dst':dstHost,'creation':10,'finish':50, 'required_BW':5 })

      self.install_only_ECMP(srcHost, dstHost, ECMP_timeout)

     # Send any waiting packets...



    elif isinstance(packet.next, arp):

      print "###################################### ARP ######################################################"

      a = packet.next

      log.debug("%i %i ARP %s %s => %s and mac is %s", dpid, inport,

       {arp.REQUEST:"request",arp.REPLY:"reply"}.get(a.opcode,

       'op:%i' % (a.opcode,)), str(a.protosrc), str(a.protodst),  packet.src)



      srcHost = a.protosrc.toSigned() & 0x00ffffff

      dstHost = a.protodst.toSigned() & 0x00ffffff

      if (srcHost, dstHost) not in self.tuple:
        self.tuple [(srcHost, dstHost)]=1
        self.list_of_elephants.append({'src':srcHost, 'dst':dstHost,'creation':10,'finish':50, 'required_BW':5 })

      self.install_only_ECMP(srcHost,dstHost,ECMP_timeout)

  

  def _handle_ConnectionUp (self, event):

    log.debug("Connection %s" % (event.connection,))

    self.switchCount += 1

    if self.switchCount==19:

      print "**********&&&&&&&&&&&&&&**************done initialization"

      #self.install_ECMP()

      #self.install_all_hedera()

  def _handle_ConnectionDown (self, event):

    log.debug("Connection %s" % (event.connection,))

    self.switchCount -= 1

    





def launch (fakeways="", arp_for_unknowns=None):

  fakeways = fakeways.replace(","," ").split()

  fakeways = [IPAddr(x) for x in fakeways]

  if arp_for_unknowns is None:

    arp_for_unknowns = len(fakeways) > 0

  else:

    arp_for_unknowns = str_to_bool(arp_for_unknowns)

  core.registerNew(Hedera_over_ECMP, fakeways, arp_for_unknowns)



