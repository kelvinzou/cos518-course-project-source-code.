# Craetes uplinks and downlinks
from link import *
def getUpLinkPorts(src_host_id):
	if src_host_id == 1:
		return [(-1,1),(3,1),(3,1)]
	elif src_host_id == 2:
		return [(-1,2),(4,1),(3,1)]
	elif src_host_id == 3:
		return [(-1,1),(3,2),(4,1)]
	elif src_host_id == 4:
		return [(-1,2),(4,2),(4,1)]
	elif src_host_id == 5:
		return [(-1,1),(3,1),(3,2)]
	elif src_host_id == 6:
		return [(-1,2),(4,1),(3,2)]
	elif src_host_id == 7:
		return [(-1,1),(3,2),(4,2)]
	elif src_host_id == 8:
		return [(-1,2),(4,2),(4,2)]
	elif src_host_id == 9:
		return [(-1,1),(3,1),(3,3)]
	elif src_host_id == 10:
		return [(-1,2),(4,1),(3,3)]
	elif src_host_id == 11:
		return [(-1,1),(3,2),(4,3)]
	elif src_host_id == 12:
		return [(-1,2),(4,2),(4,3)]
	elif src_host_id == 13:
		return [(-1,1),(3,1),(3,4)]
	elif src_host_id == 14:
		return [(-1,2),(4,1),(3,4)]
	elif src_host_id == 15:
		return [(-1,1),(3,2),(4,4)]
	elif src_host_id == 16:
		return [(-1,2),(4,2),(4,4)]

def getUpLinkSwitches(src_host_id):
	if src_host_id == 1:
		return (1,9,17)
	elif src_host_id == 2:
		return (1,10,19)
	elif src_host_id == 3:
		return (2,9,18)
	elif src_host_id == 4:
		return (2,10,20)
	elif src_host_id == 5:
		return (3,11,17)
	elif src_host_id == 6:
		return (3,12,19)
	elif src_host_id == 7:
		return (4,11,18)
	elif src_host_id == 8:
		return (4,12,20)
	elif src_host_id == 9:
		return (5,13,17)
	elif src_host_id == 10:
		return (5,14,19)
	elif src_host_id == 11:
		return (6,13,18)
	elif src_host_id == 12:
		return (6,14,20)
	elif src_host_id == 13:
		return (7,15,17)
	elif src_host_id == 14:
		return (7,16,19)
	elif src_host_id == 15:
		return (8,15,18)
	elif src_host_id == 16:
		return (8,16,20)

def getDownLinkPorts(src_host_id, sw_core_id):
	if sw_core_id == 17:
		if src_host_id == 1:
			return [(1,3),(1,3),(1,-1)]
		elif src_host_id == 2:
			return [(1,3),(1,3),(2,-1)]
		elif src_host_id == 3:
			return [(1,3),(2,3),(1,-1)]
		elif src_host_id == 4:
			return [(1,3),(2,3),(2,-1)]

		elif src_host_id == 5:
			return [(2,3),(1,3),(1,-1)]
		elif src_host_id == 6:
			return [(2,3),(1,3),(2,-1)]
		elif src_host_id == 7:
			return [(2,3),(2,3),(1,-1)]
		elif src_host_id == 8:
			return [(2,3),(2,3),(2,-1)]

		elif src_host_id == 9:
			return [(3,3),(1,3),(1,-1)]
		elif src_host_id == 10:
			return [(3,3),(1,3),(2,-1)]
		elif src_host_id == 11:
			return [(3,3),(2,3),(1,-1)]
		elif src_host_id == 12:
			return [(3,3),(2,3),(2,-1)]

		elif src_host_id == 13:
			return [(4,3),(1,3),(1,-1)]
		elif src_host_id == 14:
			return [(4,3),(1,3),(2,-1)]
		elif src_host_id == 15:
			return [(4,3),(2,3),(1,-1)]
		elif src_host_id == 16:
			return [(4,3),(2,3),(2,-1)]

	if sw_core_id == 18:
		if src_host_id == 1:
			return [(1,4),(1,3),(1,-1)]
		elif src_host_id == 2:
			return [(1,4),(1,3),(2,-1)]
		elif src_host_id == 3:
			return [(1,4),(2,3),(1,-1)]
		elif src_host_id == 4:
			return [(1,4),(2,3),(2,-1)]

		elif src_host_id == 5:
			return [(2,4),(1,3),(1,-1)]
		elif src_host_id == 6:
			return [(2,4),(1,3),(2,-1)]
		elif src_host_id == 7:
			return [(2,4),(2,3),(1,-1)]
		elif src_host_id == 8:
			return [(2,4),(2,3),(2,-1)]

		elif src_host_id == 9:
			return [(3,4),(1,3),(1,-1)]
		elif src_host_id == 10:
			return [(3,4),(1,3),(2,-1)]
		elif src_host_id == 11:
			return [(3,4),(2,3),(1,-1)]
		elif src_host_id == 12:
			return [(3,4),(2,3),(2,-1)]

		elif src_host_id == 13:
			return [(4,4),(1,3),(1,-1)]
		elif src_host_id == 14:
			return [(4,4),(1,3),(2,-1)]
		elif src_host_id == 15:
			return [(4,4),(2,3),(1,-1)]
		elif src_host_id == 16:
			return [(4,4),(2,3),(2,-1)]

	if sw_core_id == 19:
		if src_host_id == 1:
			return [(1,3),(1,4),(1,-1)]
		elif src_host_id == 2:
			return [(1,3),(1,4),(2,-1)]
		elif src_host_id == 3:
			return [(1,3),(2,4),(1,-1)]
		elif src_host_id == 4:
			return [(1,3),(2,4),(2,-1)]

		elif src_host_id == 5:
			return [(2,3),(1,4),(1,-1)]
		elif src_host_id == 6:
			return [(2,3),(1,4),(2,-1)]
		elif src_host_id == 7:
			return [(2,3),(2,4),(1,-1)]
		elif src_host_id == 8:
			return [(2,3),(2,4),(2,-1)]

		elif src_host_id == 9:
			return [(3,3),(1,4),(1,-1)]
		elif src_host_id == 10:
			return [(3,3),(1,4),(2,-1)]
		elif src_host_id == 11:
			return [(3,3),(2,4),(1,-1)]
		elif src_host_id == 12:
			return [(3,3),(2,4),(2,-1)]

		elif src_host_id == 13:
			return [(4,3),(1,4),(1,-1)]
		elif src_host_id == 14:
			return [(4,3),(1,4),(2,-1)]
		elif src_host_id == 15:
			return [(4,3),(2,4),(1,-1)]
		elif src_host_id == 16:
			return [(4,3),(2,4),(2,-1)]

	if sw_core_id == 20:
		if src_host_id == 1:
			return [(1,4),(1,4),(1,-1)]
		elif src_host_id == 2:
			return [(1,4),(1,4),(2,-1)]
		elif src_host_id == 3:
			return [(1,4),(2,4),(1,-1)]
		elif src_host_id == 4:
			return [(1,4),(2,4),(2,-1)]

		elif src_host_id == 5:
			return [(2,4),(1,4),(1,-1)]
		elif src_host_id == 6:
			return [(2,4),(1,4),(2,-1)]
		elif src_host_id == 7:
			return [(2,4),(2,4),(1,-1)]
		elif src_host_id == 8:
			return [(2,4),(2,4),(2,-1)]

		elif src_host_id == 9:
			return [(3,4),(1,4),(1,-1)]
		elif src_host_id == 10:
			return [(3,4),(1,4),(2,-1)]
		elif src_host_id == 11:
			return [(3,4),(2,4),(1,-1)]
		elif src_host_id == 12:
			return [(3,4),(2,4),(2,-1)]

		elif src_host_id == 13:
			return [(4,4),(1,4),(1,-1)]
		elif src_host_id == 14:
			return [(4,4),(1,4),(2,-1)]
		elif src_host_id == 15:
			return [(4,4),(2,4),(1,-1)]
		elif src_host_id == 16:
			return [(4,4),(2,4),(2,-1)]

def getDownLinkSwitches(src_host_id, sw_core_id):
	if (sw_core_id == 17 or sw_core_id == 18):
		if src_host_id == 1:
			return (9,1)
		elif src_host_id == 2:
			return (9,1)
		elif src_host_id == 3:
			return (9,2)
		elif src_host_id == 4:
			return (9,2)
		elif src_host_id == 5:
			return (11,3)
		elif src_host_id == 6:
			return (11,3)
		elif src_host_id == 7:
			return (11,4)
		elif src_host_id == 8:
			return (11,4)
		elif src_host_id == 9:
			return (13,5)
		elif src_host_id == 10:
			return (13,5)
		elif src_host_id == 11:
			return (13,6)
		elif src_host_id == 12:
			return (13,6)
		elif src_host_id == 13:
			return (15,7)
		elif src_host_id == 14:
			return (15,7)
		elif src_host_id == 15:
			return (15,8)
		elif src_host_id == 16:
			return (15,8)
	if (sw_core_id == 19 or sw_core_id == 20):
		if src_host_id == 1:
			return (10,1)
		elif src_host_id == 2:
			return (10,1)
		elif src_host_id == 3:
			return (10,2)
		elif src_host_id == 4:
			return (10,2)
		elif src_host_id == 5:
			return (12,3)
		elif src_host_id == 6:
			return (12,3)
		elif src_host_id == 7:
			return (12,4)
		elif src_host_id == 8:
			return (12,4)
		elif src_host_id == 9:
			return (14,5)
		elif src_host_id == 10:
			return (14,5)
		elif src_host_id == 11:
			return (14,6)
		elif src_host_id == 12:
			return (14,6)
		elif src_host_id == 13:
			return (16,7)
		elif src_host_id == 14:
			return (16,7)
		elif src_host_id == 15:
			return (16,8)
		elif src_host_id == 16:
			return (16,8)

def create_up_link(src_host_id):
	s1,s2,s3 = getUpLinkSwitches(src_host_id)
	list_of_ports = getUpLinkPorts(src_host_id)

	port1_in, port1_out = list_of_ports[0]
	port2_in, port2_out = list_of_ports[1]
	port3_in, port3_out = list_of_ports[2]
	

	link1 = Link(src =src_host_id , dst =s1, src_port=port1_in, dst_port=port1_out, capacity=10, current_use=0, owner=None, type=HS)
	link2 = Link(src =s1 , dst =s2, src_port=port2_in, dst_port=port2_out, capacity=10, current_use=0, owner=None, type=SS)
	link3 = Link(src =s2 , dst =s3, src_port=port3_in, dst_port=port3_out, capacity=10, current_use=0, owner=None, type=SS)

	return (link1, link2, link3)

def create_down_link(dst_host_id, sw_core_id):
	s1, s2 = getDownLinkSwitches(dst_host_id, sw_core_id)
	list_of_ports = getDownLinkPorts(dst_host_id, sw_core_id)

def create_best_path(src_id, dst_id):
	s1,s2,s3 = getUpLinkSwitches(src_id)
	s4, s5 = getDownLinkSwitches(dst_id, s3)

	list_of_up_ports = getUpLinkPorts(src_id)
	list_of_down_ports = getDownLinkPorts(dst_id, s3)

	port1_in, port1_out = list_of_up_ports[0]
	port2_in, port2_out = list_of_up_ports[1]
	port3_in, port3_out = list_of_up_ports[2]
	port4_in, port4_out = list_of_down_ports[0]
	port5_in, port5_out = list_of_down_ports[1]
	port6_in, port6_out = list_of_down_ports[2]

	list_of_links = []

	link1 = Link(src =src_id , dst =s1, src_port=port1_in, dst_port=port1_out, capacity=10, current_use=0, owner=None, type=HS)
	link2 = Link(src =s1 , dst =s2, src_port=port2_in, dst_port=port2_out, capacity=10, current_use=0, owner=None, type=SS)
	link3 = Link(src =s2 , dst =s3, src_port=port3_in, dst_port=port3_out, capacity=10, current_use=0, owner=None, type=SS)
	link4 = Link(src =s3, dst =s4, src_port=port4_in, dst_port=port4_out, capacity=10, current_use=0, owner=None, type=SS)
	link5 = Link(src =s4 , dst =s5, src_port=port5_in, dst_port=port5_out, capacity=10, current_use=0, owner=None, type=SS)
	link6 = Link(src =s5 , dst =dst_id, src_port=port6_in, dst_port=port6_out, capacity=10, current_use=0, owner=None, type=SH)

	list_of_links.append(link1)
	list_of_links.append(link2)
	list_of_links.append(link3)
	list_of_links.append(link4)
	list_of_links.append(link5)
	list_of_links.append(link6)

	return list_of_links

def print_best_path(src_id, dst_id):
	list_of_links = create_best_path(src_id, dst_id)
	print "this is the best path (link by link) between src %d and dst %d\n" % (src_id, dst_id)
	for link in list_of_links:
		print link


if __name__ == '__main__':
	print_best_path(1,14)
	print_best_path(14,1)



 