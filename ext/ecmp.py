#!usr/bin/env/python
#last edited by Mojgan to add the get_all_bidirectional_paths


# Jennifer Gossels
# Adapted from https://github.com/strategist/hedera/blob/master/ripl/routing.py.

import math
import random
import copy
from link import Link

class Routing(object):
    '''
    Base class for data center network routing.
    Routing engines must implement the get_route() method.
    '''

    def __init__(self, topo):
        '''
        Create Routing object.
        @param topo Topo object from Net parent
        '''

        self.topo = topo

    def get_route(self, src, dst, pkt):
        '''
        Return flow path.

        @param src source host
        @param dst destination host
        @param pkt parsed packet object
        @return flow_path list of dpids to traverse (including hosts)
        '''
        raise NotImplementedError

class StructuredRouting(Routing):
    '''
    Route flow through a FatTreeTopo and return one path.

    k pods, each with two layers of k/2 switches
    Each k port switch in edge layer connected to k/2 hosts and k/2 ports in
    aggregation layer.  Each k port switch in aggregation layer connected to
    k/2 ports in edge layer and k/2 ports in core layer.  Each k port switch
    in core layer connected to k ports in aggregation layer.

    core:  k^2/4 switches
    aggregation:  k/2 switches
    edge:  k/2 switches
    host:  k^3/4 hosts

    Host IP addresses 10.0.0.1 -- 10.0.(k^3/4) (left to right)
        Use 1 -- (k^3/4) as host identifiers.
    Edge switch IDs 1 -- (k^2/2) (left to right)
    Aggregation switch IDs (k^2/2 + 1) -- (k^2) (left to right)
    Core switch IDs (k^2 + 1) -- (k^2/4 + k^2) (left to right)

    Ports 1 -- (k/2) of each edge switch connected to hosts.  Ports (k/2 + 1) -- k
    connected to aggregation switches.
    Ports 1 -- (k/2) of each aggregation switch connected to edge switches.  Ports
    (k/2 + 1) -- k connected to core switches.
    First k/2 core switches connected to first aggregation switch in each pod,
    second k/2 core switches connected to second aggregation switch in each pod, ...,
    (k/2)th (last) k/2 core switches connected to (k/2)th (last) aggregation switch
    in each pod.
    '''

    def __init__(self, k):
        '''
        Create Routing object.
        
        @param k value of k used for FatTreeTopo
        '''
        self.k = k
        # name some constants
        self.hMIN = 1
        self.hMAX = (self.k**3)/4
        self.eMIN = 1
        self.eMAX = (self.k**2)/2
        self.aMIN = self.eMAX + 1
        self.aMAX = self.k**2
        self.cMIN = self.aMAX + 1
        self.cMAX = (self.k/2)**2 + (self.k)**2

        # Map each (src, dst) pair to a list of all paths between them.
        self.paths = {}
        # Pre-populate dictionary with (src, dest) keys.
        for src in range(self.hMIN, self.hMAX + 1):
            for dst in range(self.hMIN, self.hMAX + 1):
                if not (src == dst):
                    self.paths[(src, dst)] = []
        self.enumerate_paths()

    def get_path(self, src, dst):
        '''
        Randomly choose a path from the list of all possible paths from src to dst.
        '''
        if src == dst:
            return []
        else:
            return random.choice(self.paths[(src, dst)])
    def reverse_all_paths(self, paths):
        rev_paths = []
        for path in paths:
            rev_paths.append(self.reverse_path(path, True))
        return rev_paths

    def get_all_bidirectional_paths(self, src,dst):
        if src == dst :
            return []
        else:
	    return_list = []

            paths = self.paths[(src,dst)]

	    for path in paths:
		return_list.append((path,self.reverse_path(path, True)))

            return return_list
    def get_bidirectional_path(self, src, dst):
        '''
        Randomly choose a path from the list of all possible paths from src to dst.
        Return this path, along with this path in reverse to be the path from dst
        to src.
        '''
        path = self.get_path(src, dst)
        reverse = self.reverse_path(path, True)
        return (path, reverse)

    def enumerate_paths(self):
        for dst in range(self.hMIN, self.hMAX + 1):
            (dst_edge, dst_host_edge_port) = self.edge_from_host(dst)
            e_d = Link(dst_edge, dst, src_port=dst_host_edge_port, type="sh")
            for src in range(self.hMIN, self.hMAX + 1):
                # skip if src = dst
                if src == dst:
                    continue
                (src_edge, src_host_edge_port) = self.edge_from_host(src)
                # Link representing first hop on this path
                s_e = Link(src, src_edge, dst_port=src_host_edge_port, type="hs")           
                # only one path between hosts connected to same edge switch
                if src_edge == dst_edge:
                    this_path = [s_e, e_d]
                    self.paths[(src, dst)].append(this_path)
                else:
                    # all aggregation switches connected to dst edge switch
                    for dst_edge_agg_port in range(self.k/2 + 1, self.k + 1):
                        (dst_agg_switch, dst_agg_edge_port) = \
                            self.up_to_agg(dst_edge, dst_edge_agg_port)
                        # Link representing hop from dst_agg to dst_edge on this path
                        da_de = Link(dst_agg_switch, dst_edge, src_port=dst_agg_edge_port,
                                     dst_port=dst_edge_agg_port)
                        if self.same_pod(src, dst):
                            (src_agg_edge_port, src_edge_agg_port) = \
                                self.down_from_agg(dst_agg_switch, src_edge)
                            se_sa = Link(src_edge, dst_agg_switch, src_port=src_edge_agg_port, \
                                             dst_port=src_agg_edge_port)
                            this_path = [s_e, se_sa, da_de, e_d]
                            self.paths[(src, dst)].append(this_path)
                        else:
                            # all core switches connected to this agg switch
                            for dst_agg_core_port in range(self.k/2 + 1, self.k + 1):
                                (core_switch, dst_core_agg_port) = \
                                    self.up_to_core(dst_agg_switch, dst_agg_core_port)
                                c_da = Link(core_switch, dst_agg_switch, src_port=dst_core_agg_port, \
                                                dst_port=dst_agg_core_port)
                                (src_agg_switch, src_agg_core_port, src_core_agg_port) = \
                                    self.agg_of_host_core(src, core_switch)
                                sa_c = Link(src_agg_switch, core_switch, src_port=src_agg_core_port, \
                                                dst_port=src_core_agg_port)
                                (src_agg_edge_port, src_edge_agg_port) = \
                                    self.down_from_agg(src_agg_switch, src_edge)
                                se_sa = Link(src_edge, src_agg_switch, src_port=src_edge_agg_port, \
                                                 dst_port=src_agg_edge_port)
                                this_path = [s_e, se_sa, sa_c, c_da, da_de, e_d]
                                self.paths[(src, dst)].append(this_path)
                        
    def reverse_path(self, path, bidirectional):
        '''
        Reverse path.  bidirectional is a boolean indicating whether usage
        and capacity characteristics of each link in the reversed path should
        be the same as those in path.
        '''
        rev = []
        for lnk in reversed(path):
            rev.append(lnk.reverse(bidirectional))
        return rev
                        
    def edge_in_pod(self, edge):
        '''
        Number of this edge switch within its pod.
        '''
        # convert edge ID to integer in 0 -- (k/2 - 1)
        normalized = edge - self.eMIN
        return (normalized % (self.k/2)) + 1

    def agg_in_pod(self, agg):
        '''
        Number of this aggregation switch within its pod.
        '''
        # convert agg ID to integer in 0 -- (k/2 - 1)
        normalized = agg - self.aMIN
        return (normalized % (self.k/2)) + 1

    def core_in_group(self, core):
        '''
        Number of this core switch within its group of k/2 core switches
        that connect to the same aggregation switch in each pod.
        '''
        normalized = core - self.cMIN
        return (normalized % (self.k/2)) + 1

    def get_agg_in_pod(self, agg_in_pod, pod):
        '''
        ID of the (agg_in_pod)th aggregation switch in pod.
        '''
        return self.aMIN + (self.k/2) * (pod - 1) + (agg_in_pod - 1)
    
    def pod_of_edge(self, edge):
        normalized = edge - self.eMIN
        # k/2 edge switches in each pod
        previous = normalized / (self.k/2)
        return previous + 1

    def pod_of_agg(self, agg):
        normalized = agg - self.aMIN
        previous = normalized / (self.k/2)
        return previous + 1
    
    def edge_from_host(self, host):
        '''
        Edge switch (ID, port) connected to host (i.e. first element is
        integer in eMIN -- eMAX, second element is integer in 1 -- k/2).

        @param host ID of host (i.e. integer in hMIN -- hMAX)
        '''
        # each edge switch connected to k/2 hosts
        switch_offset = (host-1) / (self.k/2)
        switch = self.eMIN + switch_offset
        # (k/2) * switch_offset hosts connected to other switches
        port = host - (self.k/2) * switch_offset
        return (switch, port)

    def edge_from_agg(self, agg_switch, agg_port):
        '''
        Edge switch (ID, port) connected to aggregation switch via link from
        agg_port.

        @param agg_switch ID of aggregation switch (i.e. integer in aMIN -- aMAX)
        @param agg_port port number of link on aggregation switch
        '''
        pod = self.pod_of_agg(agg_switch)
        edge_switch = self.eMIN + (pod - 1) * (self.k/2) + agg_port
        # edge switch uses its first k/2 ports for connections to hosts
        edge_port = self.agg_in_pod(agg_switch) + self.k/2
        return(edge_switch, edge_port)

    def down_from_agg(self, agg_switch, edge_switch):
        '''
        (agg switch port, edge switch port) connecting agg_switch and edge_switch.

        @param agg_switch ID of agg switch (i.e. integer in aMIN -- aMAX)
        @param edge_switch ID of edge switch (i.e. integer in eMIN -- eMAX)
        '''
        agg_port = self.edge_in_pod(edge_switch)
        edge_port = self.agg_in_pod(agg_switch) + (self.k/2)
        return(agg_port, edge_port)

    def up_to_agg(self, edge_switch, edge_port):
        '''
        Aggregation switch (ID, port) connected to edge switch via link from
        edge_port.

        @param edge_switch ID of edge switch (i.e. integer in eMIN -- eMAX)
        @param edge_port port number of link on edge switch
        '''
        pod = self.pod_of_edge(edge_switch)
        # last agg switch in previous pod
        previous_agg = self.eMAX + (pod - 1) * (self.k/2)
        agg_switch = previous_agg + edge_port - self.k/2
        agg_port = self.edge_in_pod(edge_switch)
        return (agg_switch, agg_port)
          
    def core_group(self, agg):
        '''
        List of IDs of core switches connected to agg.
        '''
        group_number = self.agg_in_pod(agg) - 1
        start = self.cMIN + self.k/2 * group_number
        end = start + self.k/2
        return range(start, end + 1)

    def up_to_core(self, agg_switch, agg_port):
        '''
        Core switch (ID, port) connected to agg switch via link from agg_port.

        @param agg_switch ID of aggregation switch (i.e. integer in aMIN -- aMAX)
        @param agg_port port number of link on aggregation switch
        '''
        normalized = agg_port - self.k/2
        core_switch = self.core_group(agg_switch)[normalized - 1]
        core_port = self.pod_of_agg(agg_switch)
        return(core_switch, core_port)

    def agg_of_host_core(self, host, core):
        '''
        ID of aggregation switch connecting host to core switch, and port
        on agg switch connecting to core, and port on core switch connecting
        to agg.
        '''
        # renumber core switches to range 0 -- k^2/4 - 1
        normalized = core - self.aMAX
        group_number = int(math.ceil(float(normalized) / (self.k/2)))
 
        pod = self.pod_of_host(host)
        agg_switch = self.get_agg_in_pod(group_number, pod)
        src_agg_core_port = self.k/2 + self.core_in_group(core)
        # src_core_agg_port is just src's pod number
        return (agg_switch, src_agg_core_port, pod)

    def pod_of_host(self, host):
        '''
        Pod number of host.  Assume left to right numbering, 1 -- k.
        
        @param host ID of host (i.e. integer in hMIN -- hMAX)
        '''
        # Each pod has (k^3/4)/k = (k/2)^2 hosts.
        return int(math.ceil(float(host) / ((self.k/2)**2)))

    def same_edge(self, h1, h2):
        '''
        h1 and h2 are connected to the same edge switch.

        @param h1 ID of one of the hosts (i.e. integer in hMIN -- hMAX)
        @param h2 ID of other host
        '''
        return self.edge_from_host(h1) == self.edge_from_host(h2)

    def same_pod(self, h1, h2):
        '''
        h1 and h2 are in the same pod.

        @param h1 ID of one of the hosts (i.e. integer in hMIN -- hMAX)
        @param h2 ID of other host
        '''
        return self.pod_of_host(h1) == self.pod_of_host(h2)

    '''
    Testing functions have values hard coded for k = 4.
    '''
    def test(self, src, dst, edge_switch, edge_port, agg_switch, agg_port):
        if (edge_switch, edge_port) == (1,3):
            assert((agg_switch, agg_port) == (9,1))
        elif (edge_switch, edge_port) == (1,4):
            assert((agg_switch, agg_port) == (10,1))
        elif (edge_switch, edge_port) == (2,3):
            print "src:  ",
            print src
            print "dst:  ",
            print dst
            print "edge switch:  ",
            print edge_switch
            print "edge port:  ",
            print edge_port
            print "agg switch:  ",
            print agg_switch
            print "agg port:  ",
            print agg_port
            print "\n"
            assert((agg_switch, agg_port) == (9,2))
        elif (edge_switch, edge_port) == (2,4):
            assert((agg_switch, agg_port) == (10,2))
        elif (edge_switch, edge_port) == (3,3):
            assert((agg_switch, agg_port) == (11,1))
        elif (edge_switch, edge_port) == (3,4):
            assert((agg_switch, agg_port) == (12,1))
        elif (edge_switch, edge_port) == (4,3):
            assert((agg_switch, agg_port) == (11,2))
        elif (edge_switch, edge_port) == (4,4):
            assert((agg_switch, agg_port) == (12,2))
        elif (edge_switch, edge_port) == (5,3):
            assert((agg_switch, agg_port) == (13,1))
        elif (edge_switch, edge_port) == (5,4):
            assert((agg_switch, agg_port) == (14,1))
        elif (edge_switch, edge_port) == (6,3):
            assert((agg_switch, agg_port) == (13,2))
        elif (edge_switch, edge_port) == (6,4):
            assert((agg_switch, agg_port) == (14,2))
        elif (edge_switch, edge_port) == (7,3):
            assert((agg_switch, agg_port) == (15,1))
        elif (edge_switch, edge_port) == (7,4):
            assert((agg_switch, agg_port) == (16,1))
        elif (edge_switch, edge_port) == (8,3):
            assert((agg_switch, agg_port) == (15,2))
        elif (edge_switch, edge_port) == (8,4):
            assert((agg_switch, agg_port) == (16,2))

    def test_agg_of_host_core(self, host, core, agg_switch):
        if host in range(1,5):
            if (core == 17) or (core == 18):
                assert(agg_switch == 9)
            elif (core == 19) or (core == 20):
                assert(agg_switch == 10)
        elif host in range(5, 9):
            if (core == 17) or (core == 18):
                assert(agg_switch == 11)
            elif (core == 19) or (core == 20):
                assert(agg_switch == 12)
        elif host in range(5, 9):
            if (core == 17) or (core == 18):
                assert(agg_switch == 13)
            elif (core == 19) or (core == 20):
                assert(agg_switch == 14)
        elif host in range(9, 13):
            if (core == 17) or (core == 18):
                assert(agg_switch == 13)
            elif (core == 19) or (core == 20):
                assert(agg_switch == 14)
        elif host in range(13, 17):
            if (core == 17) or (core == 18):
                assert(agg_switch == 15)
            elif (core == 19) or (core == 20):
                assert(agg_switch == 16)
       

def main():
    router = StructuredRouting(4)
    '''
    for (src, dst) in router.paths:
        if src < dst:
            (src_dst, dst_src) = router.get_bidirectional_path(src, dst)
            for lnk in src_dst:
                print str(lnk)
            for lnk in dst_src:
                print str(lnk)
    '''
    print router.paths[(2,16)]
    print "Paths from H2 to H16:  \n"
    for path in router.paths[(2, 16)]:
        print "---------------------------\n"
        for lnk in path:
            print str(lnk)
    print  "---------------------------\n"
    print "Paths from H16 to H2:  \n"
    for path in router.paths[(16, 2)]:
        print  "---------------------------\n"
        for lnk in path:
            print str(lnk)
    
if __name__ == "__main__":
    main()
